package com.example.student.snapchat;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.util.Log;


public class PhotofriendService extends IntentService {


    public PhotofriendService() {
        super("PhotofriendService");
    }



    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            String action = intent.getAction();
            if (action.equals(Constants.ACTION_ADD_FRIEND)){
                Log.i("PhotofriendService" , "Service adding friend.");
            }
        }
    }


}
