package com.example.student.snapchat;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

import java.util.ArrayList;
import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class FriendsFragment extends Fragment {
    private ArrayList<String> friends;
    private ArrayAdapter<String> friendListAdapter;


    public FriendsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_friends, container, false);

        friends = new ArrayList<String>();
        friendListAdapter = new ArrayAdapter<String>(
                getActivity(), android.R.layout.simple_list_item_1, friends);

        ListView friendList = (ListView) view.findViewById(R.id.friendList);
        friendList.setAdapter(friendListAdapter);

        String currentUser = Backendless.UserService.loggedInUser();
        Backendless.Persistence.of(BackendlessUser.class).findById(currentUser, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser user) {
                Object[] friendObjects = (Object[]) user.getProperty("Friends");
                if (friendObjects.length > 0) {
                    BackendlessUser[] friendArray = (BackendlessUser[]) friendObjects;
                    for (BackendlessUser friend : friendArray) {
                        String name = friend.getProperty("name").toString();
                        friends.add(name);
                        friendListAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {

            }
        });
        Button addfriend = (Button) view.findViewById(R.id.addfriend);
        addfriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setMessage("Add a Friend.");

                final EditText inputField = new EditText(getActivity());
                alertDialog.setView(inputField);

                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        addFriend(inputField.getText().toString());
                        Toast.makeText(getActivity(), "Add Friend!", Toast.LENGTH_SHORT).show();
                    }
                });

                alertDialog.create();
                alertDialog.show();
            }
        });

        return view;

    }
        private void addFriend(String friendName){
            Intent intent = new Intent(getActivity(),PhotofriendService.class);
            intent.setAction(Constants.ACTION_ADD_FRIEND);

            getActivity().startService(intent);
        }

}
